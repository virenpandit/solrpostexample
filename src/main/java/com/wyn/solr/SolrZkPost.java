 package com.wyn.solr;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import java.util.Iterator;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

public class SolrZkPost {

    private CloudSolrServer zkServer = null;
    
    private static String ZK_URL = "vsvphxslrdev01.hotelgroup.com:9095";
    private static String COLLECTION_NAME = "perf";
    private static int ITERATIONS = 10;


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static void log(Object o) {
        String date = sdf.format(new Date(System.currentTimeMillis()));
        System.out.println(date + ": " + o);
    }
    
    private static void loadArgs(String args[]) {
        if(args.length==3) {
            ZK_URL = args[0];
            COLLECTION_NAME = args[1];
            ITERATIONS = Integer.parseInt(args[2]);
        }
        
        log("Using Zookeeper Url: " + ZK_URL);
        log("Using Collection: " + COLLECTION_NAME);
        log("Using number of iterations: " + ITERATIONS);
    }
    
    private void printUsage() {
        log("java com.wyn.solr.SolrZkPost zk-host:port collection iterations");
    }

    public static void main(String args[]) throws Exception {

        loadArgs(args);

        SolrZkPost solrZkPost = new SolrZkPost();
        solrZkPost.run();
    }
    
    public void run() throws Exception {
        connect();
        for(int i=1; i<=ITERATIONS; i++ ) {
            log("Saving to Solr - Attempt: " + i);
            save();
        }
    }

    public void connect() throws Exception {
        if(null==zkServer) {
            log("Connecting to Zookeeper URL: [" + ZK_URL + "]");
            zkServer = new CloudSolrServer(ZK_URL);
            zkServer.connect();
        } else {
            log("Already connected to Zookeeper...");
        }
    }
    public void disconnect() throws Exception {
        zkServer.shutdown();
    }

    public void save() {
        log("Inside: save().v.2.2");
        try {
            
            zkServer.setDefaultCollection(COLLECTION_NAME);
            
            SolrInputDocument document = getRecord();

            zkServer.add(document, 5000);
            log("Document saved with id: " + document.getField("id"));

        } catch(Exception ex) {
            log("Exception in save()");
            ex.printStackTrace();
            try {
                zkServer.shutdown();
            } catch(Exception ex2) { }
        }
        log("Exiting: save().v.1.2");
    }
    
    public SolrInputDocument getRecord() {
        SolrInputDocument document = new SolrInputDocument();
        String randomId = getUUID();

        document.addField("id", getUUID());
        document.addField("GNR_NUM", getRandomString(8));
        document.addField("CONFIRMATION_NO", getRandomString(4));
        document.addField("CANCELLATION_NO", getRandomString(4));
        document.addField("BRAND_ID", "RA");
        document.addField("SITE_ID", getRandomString(5));
        document.addField("MEMBER_NUM", getRandomString(16));
        document.addField("BOOKING_TS", "2015-02-17T12:49:21Z");
        document.addField("RESV_STATUS", "NEW RESERVATION");
        document.addField("CHANNEL", getRandomString(2));
        document.addField("CHECK_IN_DATE", "2015-02-17T12:49:21Z");
        document.addField("LOS", 1);
        document.addField("POINTS_USED", 0);
        document.addField("TOTAL_TAX", getRandomString(2));
        document.addField("NO_OF_ROOMS", 2);
        document.addField("AWARD_NUMBER", getRandomString(12));
        document.addField("ADULTS", getRandomString(1));
        document.addField("CHILDREN", getRandomString(1));
        document.addField("CHILDREN_18", getRandomString(1));
        document.addField("GUEST_NAME", getRandomString(18));
        document.addField("GUEST_FIRST_NAME", getRandomString(7));
        document.addField("GUEST_MIDDLE_NAME", getRandomString(3));
        document.addField("GUEST_LAST_NAME", getRandomString(8));
        document.addField("GUEST_ADDRESS", "25 " + getRandomString(7) + " Street");
        document.addField("CITY", getRandomString(2));
        document.addField("STATE", getRandomString(2));
        document.addField("COUNTRY", "USA");
        document.addField("POSTAL_CODE", getRandomString(5));
        document.addField("RATE_PLAN", "SF" + getRandomString(1));
        document.addField("ROOM_TYPE", getRandomString(3) + "1");
        document.addField("ROOM_REVENUE", getRandomString(3));
        document.addField("ROOM_TITLE", "King Suite");
        document.addField("RATE_SUPPRESSION_FLAG", 0);
        document.addField("src", "Mart");
        document.addField("ind_ts", "2015-02-17T12:49:21Z");

        return document;
    }

    public String getUUID() {
        return UUID.randomUUID().toString();
    }
    public String getRandomString(int len) {
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        return s.substring(0, len);
    }
    public String getRandomUrl() {
        String s = getRandomString(1950);
        return "http://www.google.com?data=" + s;
    }
}
