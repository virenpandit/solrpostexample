package com.wyn.solr;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.util.HashMap;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.JSONArray;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.methods.StringRequestEntity;


/**
 * Implementation of Solr Search.
 */

public class SolrSearchPublish {

    /** Default log. */
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    /* ChangeMe */
    private final static String SOLR_URL = "http://localhost:810983/solr/aemcontent/update/json?commit=true";

    public void upload() throws WorkflowException {

        try {
            log.info("Entering execute()");

            JSONArray allRecords = new JSONArray();
            JSONObject jsonRecord = nodePropertiesAsJson(node);

            if(null!=jsonRecord) {
                allRecords.put(jsonRecord);
            }

            if(null!=allRecords) {
                sendToSolrSearch(allRecords);
            }

            log.info("Exit: execute()");
        } catch(Exception ex) {
            throw new WorkflowException(ex);
        }
    }


    /*
     * Get Page properties as JSON
     * TODO - Match against Solr attributes
     *
     */
    private JSONObject nodePropertiesAsJson(Node node) throws Exception {

        log.info("Entering: nodePropertiesAsJson()");

        javax.jcr.PropertyIterator iterator = node.getProperties();

        JSONObject jsonRecord = new JSONObject();
        if(iterator.hasNext()) {

            while(iterator.hasNext()) {
                javax.jcr.Property prop = (javax.jcr.Property)iterator.nextProperty();
                if((prop.getName().startsWith("jcr:uuid")) || (!prop.getName().startsWith("jcr:") && !prop.getName().startsWith("sling:") && !prop.getName().startsWith("cq:"))) {
                    if(!prop.isMultiple()) {
                        String name = prop.getName();
                        if(StringUtils.equals("jcr:uuid", name))
                            name = "id";
                        String value = (String)prop.getValue().getString();
                        if(!StringUtils.isEmpty(name) && !StringUtils.isEmpty(value)) {
                            jsonRecord.put(name, value);
                        }
                    }
                }
            }
        }

        if(jsonRecord.length()>0) {
            log.info("jsonRecord=" + jsonRecord.toString());
            jsonRecord.put("url", AEM_HOST + node.getPath());
        } else {
            log.info("Returning empty jsonObject");
        }

        log.info("Exit: nodePropertiesAsJson()");

        return jsonRecord;
    }


    private void sendToSolrSearch(JSONArray allRecords) throws Exception {

        log.info("Entering: sendToSolrSearch()");
        log.info("JSONArray: allRecords=" + allRecords.toString());

        HttpClient httpClient = new HttpClient();
        StringRequestEntity requestEntity = new StringRequestEntity(allRecords.toString(), "application/json", "UTF-8");
        PostMethod method = new PostMethod(SOLR_URL);
        //HttpClientParams params = new HttpClientParams();
        //params.setParameter("commit", "true");
        method.setRequestEntity(requestEntity);
        int statusCode = httpClient.executeMethod(method);

        log.info("Solr Json post returned: " + statusCode);

        log.info("Exiting: sendToSolrSearch()");
    }



    /*
        SearchResult result = executeQuery(workItem, wfSession, metadata);
        resultAsJson(result, wfSession);
        Http Equivalent (Recursive):
            http://localhost:4502/bin/querybuilder.json?p.hits=full&group.1_path=/content/myportal-tny1/en/supernav/support/jcr:content&p.nodedepth=5&property=jcr%3atitle
    */
    public SearchResult executeQuery(WorkItem workItem, WorkflowSession wfsession, MetaDataMap metadata) throws Exception {

        log.info("Entering: executeQuery()");
        ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        Session session = resourceResolver.adaptTo(Session.class);

        String nodePath = workItem.getWorkflowData().getPayload().toString() + "/jcr:content";
        log.info("nodePath=" + nodePath);
        log.info("queryBuilder=" + queryBuilder);

        Map<String, String> map = new HashMap<String, String>();
        map.put("p.hits", "full");
        map.put("group.1_path", nodePath);
        map.put("p.nodedepth", "5");
        map.put("property", "jcr:title");
        map.put("p.limit", "-1");

        Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
        query.setStart(0);
        log.info("Executing query: " + query.toString());
        SearchResult result = query.getResult();
        log.info("SearchResults: " + result);

        log.info("Exit: executeQuery()");

        return result;
    }


    /*
     * Iterates through children for page content node, adding their attributes to jsonRecord
     */
    private String pagePropertiesRecursiveAsJson(SearchResult result, WorkflowSession wfsession) throws Exception {

        log.info("Entering: pagePropertiesRecursiveAsJson()");

        String jsonData = null;

        Session session = wfsession.getSession();

        // paging metadata
        int hitsPerPage = result.getHits().size();
        long totalMatches = result.getTotalMatches();

        log.info("Total hits=" + hitsPerPage + ", Total Matches=" + totalMatches);

        JSONArray allRecords = new JSONArray();
        for (Hit hit : result.getHits()) {
            String path = hit.getPath();
            Node node = session.getNode(path);

            if(node.hasProperty("sling:resourceType")) {
                String nodeResourceType = "" + node.getProperty("sling:resourceType");

                if(!nodeResourceType.endsWith("parsys")) {

                    log.info("nodeResourceType=" + nodeResourceType + ", Hit.path=" + path + ", Hit.title=" + hit.getTitle());
                    javax.jcr.PropertyIterator iterator = node.getProperties();

                    if(iterator.hasNext()) {
                        JSONObject jsonRecord = null;
                        while(iterator.hasNext()) {
                            javax.jcr.Property prop = (javax.jcr.Property)iterator.nextProperty();
                            if(!prop.getName().startsWith("jcr:") && !prop.getName().startsWith("sling:") && !prop.getName().startsWith("cq:")) {
                                if(!prop.isMultiple()) {

                                    String name = prop.getName();
                                    String value = (String)prop.getValue().getString();
                                    if(!StringUtils.isEmpty(name) && !StringUtils.isEmpty(value)) {
                                        jsonRecord = new JSONObject();
                                        jsonRecord.put(name, value);
                                    }
                                }
                            }
                        }
                        if(jsonRecord!=null)
                            allRecords.put(jsonRecord);
                    }
                }
            }
        }
        log.info("JSON=" + allRecords.toString());

        session.save();
        log.info("Exit: pagePropertiesRecursiveAsJson(jsonData=" + jsonData + ")");

        return jsonData;
    }
}
