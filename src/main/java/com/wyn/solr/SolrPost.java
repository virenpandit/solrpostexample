package com.wyn.solr;

import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;

public class SolrPost {

    public static void log(Object o) {
        System.out.println("" + o);
    }

    public static void main(String args[]) {
        new SolrPost().upload();
    }

    public void upload() {
        try {

            SolrServer server = new HttpSolrServer("http://localhost:10983/solr/aemcontent");

            int max = 50000;
            int i=0;
            for(i=0;i<max;i++) {
                SolrInputDocument doc1 = new SolrInputDocument();
                String randomId = getUUID();
                doc1.addField("id", randomId);
                doc1.addField("headline", getRandomString(500));
                doc1.addField("publish_dt", new Date());
                doc1.addField("expiry_dt", new Date());
                doc1.addField("category", getRandomString(500));
                doc1.addField("brand", getRandomString(150));
                doc1.addField("region", getRandomString(100));
                doc1.addField("role", getRandomString(500));
                doc1.addField("tags", getRandomString(750));
                doc1.addField("short_desc", getRandomString(1000));
                doc1.addField("long_desc", getRandomString(5000));
                doc1.addField("img_thumbnail", getRandomUrl());
                doc1.addField("img_medium", getRandomUrl());
                doc1.addField("img_large", getRandomUrl());
                doc1.addField("is_featured", true);
                doc1.addField("url", getRandomUrl());
                doc1.addField("language", getRandomString(10));

                Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
                docs.add(doc1);
                //System.out.println(docs.toString());
                UpdateResponse response = server.add(docs);
                log("Posted document to Solr:ID [" + randomId + "]");

                if(i%10 == 0) {
                    log("Committing updates: " + i + "/" + max);
                    server.commit();
                }
            }
            server.commit();
            log("Last commit done! Total documents uploaded: " + i + "/" + max);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getRandomUrl() {
        String s = getRandomString(1950);
        return "http://www.google.com?data=" + s;
    }

    public String getUUID() {
        return UUID.randomUUID().toString();
    }
    public String getRandomString(int len) {
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        return s.substring(0, len);
    }
}
