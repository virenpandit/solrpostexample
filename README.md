mvn archetype:generate -DgroupId=com.wyn.solr -DartifactId=SolrApp -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

To RUN:
USE:
use Maven shade plug-in to include JAR dependencies in final JAR-file
set classpath=.\target\uber-SolrApp-1.0-SNAPSHOT.jar;%classpath%

mvn clean package
java com.wyn.solr.SolrPost

OR
To use Maven exec:java
mvn test
